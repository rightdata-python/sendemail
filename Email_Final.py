import os.path
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from fastapi import FastAPI

app = FastAPI()

def send_email(subject, message, from_email, password ,to_email=[], attachment=[]):
    """
    :param subject: email subject
    :param message: Body content of the email (string), can be HTML/CSS or plain text
    :param from_email: Email address from where the email is sent
    :param to_email: List of email recipients, example: ["a@a.com", "b@b.com"]
    :param attachment: List of attachments, exmaple: ["file1.txt", "file2.txt"]
    """
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = from_email
    msg['password'] = password
    msg['To'] = ", ".join(to_email)
    msg.attach(MIMEText(message, 'html'))

    for f in attachment:
        with open(f, 'rb') as a_file:
            basename = os.path.basename(f)
            part = MIMEApplication(a_file.read(), Name=basename)

        part['Content-Disposition'] = 'attachment; filename="%s"' % basename
        msg.attach(part)

    # email = smtplib.SMTP('smtp.gmail.com', 587)
    # email.login(from_email, password)
    # email.sendmail(from_email, to_email, msg.as_string())

    # server = smtplib.SMTP('smtp.gmail.com', 25)
    server = smtplib.SMTP('smtp.gmail.com', 587)
    # server.connect("smtp.example.com", 465)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(from_email, password)
    text = msg.as_string()
    server.sendmail(from_email, to_email, text)
    server.quit()

# send = send_email("hi this is subject","thisis message","alla.bhavishya@gmail.com","vtgpckdrarcnmlzx",["bhavishyaalla8@gmail.com","alla.bhavishya@gmail.com"],["C:/Users/gvsph/PycharmProjects/Flask_App/jwtauthtest/details.csv"])
# print(send)

# if __name__ == '__main__':
#     send_email("hi this is subject","thisis message","alla.bhavishya@gmail.com","vtgpckdrarcnmlzx",["bhavishyaalla8@gmail.com","alla.bhavishya@gmail.com"],["C:/Users/gvsph/PycharmProjects/Flask_App/jwtauthtest/details.csv"])


