from fastapi import FastAPI
from pydantic import BaseModel
from Email_Final import send_email

app = FastAPI()

class EmailRequest(BaseModel):
    subject: str
    message: str
    from_email: str
    password : str
    to_email : list
    attachment : list

@app.post("/EmailRequest")

async def EmailRequest(em:EmailRequest):
    Result = send_email(em.subject,em.message,em.from_email,em.password,em.to_email,em.attachment)
    return {f"{Result}"}
